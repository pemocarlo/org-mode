#+TITLE: Automate the boring stuff with python
#+AUTHOR: Carlos P�rez


#+BEGIN_SRC python :eval never
print("hello world")
name = input() 
#+END_SRC

* Flow control  

* Functions
** Practice projects

#+BEGIN_SRC python :tangle scripts/collatz_sequence.py

def collatz(number):
    """
    Implements and prints the collatz sequence
    """
    # Recursive implementation. Always prints value of current step
    value = None  # stores the current value of the sequence for any step

    # 1 is the exhaustive value
    if number == 1:
        return number

    # Two recursive values
    # First case when number is even
    elif number % 2 == 0:
        value = number // 2
        print(value)
        return collatz(value)
    # Second case when number is odd
    elif number % 2 != 0:
        value = 3 * number + 1
        print(value)
        return collatz(value)


def collatz_interactive():
    """
    Asks the user to insert a number to start a collatz sequence
    """
    try:
        val = int(input("Enter an integer to get its collatz sequence: "))
    except ValueError:
        print("Please enter an integer")
        collatz_interactive()
    else:
        print(val)
        collatz(val)

#+END_SRC

* Lists
Can contain multiple values. Useful to arrange data in hierarchichal
structures. 

** Slicing
To get several values form a list into a mew list.

list[first included: first not included]

As a shortcut, you can leave out one or both of the indexes on either side of
the colon in the slice. Leaving out the first index is the same as using 0 , or
the beginning of the list. Leaving out the second index is the same as using
the length of the list, which will slice to the end of the list

** Concatenation and replication 
The + operator: combine two lists to create a new one like string
concatenation. 
The * operator: replicate the list a number of times

** Removing values
~del~ statement to delete an item of a list. Also with simple variables.
~del list[2]~

** Working with lists

*** Enumeration
  #+BEGIN_SRC python
animals = ["dog", "cat", "monkey", "lion", "elephant", "bear"]

for index, element in enumerate(animals):
    print ('{} {}'.format(index, element))

  #+END_SRC

** Methods
The same thing as a function, except it's "called" on a value. Each data type
has his own set of methods.

 - index(val): Returns the index of a value in the list. If it does not exist,
   returns ValueError. In case of duplicates, returns the index of the first.
 - append(val): adds arg to the end of the list. Returns None
 - insert(index, val) : insert a value at any index. Returns None
 - remove(val): remove when you know the value.
 - sort(reverse=False): sorts the list in place, cannot sort lists with strings
   and numbers, strings sorted in ASCIIbetical order.

** List like types: strings and tuples
   
** References
When you assign a list to a variable, you are actually assigning a list
reference to the variable. A reference is a value that points to some bit of
data, and a list reference is a value that points to a list.

** Practice projects

#+BEGIN_SRC python

def list_to_text(list):
    """
    Converts a list to a string containing all the elements
    Keyword Arguments:
    list -- any list
    Returns a string enumerating the items of list.
    """
    # Use join(iterable) method of strings. Assure conversion to string
    sentence = ", ".join(str(item) for item in list[:-1])
    # Treats the last element of the list individually to add "and"
    sentence += " and " + list[-1]
    return sentence
#+END_SRC


#+BEGIN_SRC python

grid = [['.', '.', '.', '.', '.', '.'],
        ['.', 'O', 'O', '.', '.', '.'],
        ['O', 'O', 'O', 'O', '.', '.'],
        ['O', 'O', 'O', 'O', 'O', '.'],
        ['.', 'O', 'O', 'O', 'O', 'O'],
        ['O', 'O', 'O', 'O', 'O', '.'],
        ['O', 'O', 'O', 'O', '.', '.'],
        ['.', 'O', 'O', '.', '.', '.'],
        ['.', '.', '.', '.', '.', '.']]


print("\n")
for x in range(6):
    for y in range(9):
        print(grid[y][x], end="")
    else:
        print("\n")
#+END_SRC
* Dictionaries and structuring data
Dictionaries are collection of many values. Indexes, called keys, can be any
inmutable value.

** Methods
 - keys()
 - values()
 - items()
 - get(key, default_if_key_not_found)
 - setdefault(key, default_value): set a value in a dictionary for a certain
   key only if that key does not already have a value.

#+BEGIN_SRC python
"""
Short program that counts the number of occurences of each letter in a string.
"""

import pprint
message = "It was a bright cold day in April, and the clocks" + \
    "were striking thirteen."
count = {}  # dictionary with characters as keys and counts as values
for character in message:
    # if chr key not found, add it when 0 as val
    count.setdefault(character, 0)
    count[character] += 1  # counter

print("\n")
# use pretty print to show a formatted dictionary string with ordered keys
pprint.pprint(count)
#+END_SRC

** Using data structures to model real-world things

*** Tic-tac-toe example
The board looks like a large hash symbol #. It contains 9 slots, that can
contain X or O.
To represent the board we can use a dictionary, assigning each slot a key-value pair.

#+BEGIN_SRC python :tangle scripts/tictactoe.py

# clear board
theBoard = {'top-L': ' ', 'top-M': ' ', 'top-R': ' ',
            'mid-L': ' ', 'mid-M': ' ', 'mid-R': ' ',
            'low-L': ' ', 'low-M': ' ', 'low-R': ' '}

def printBoard(board):
    print("\n")
    print(board['top-L'] + '|' + board['top-M'] + '|' + board['top-R'])
    print('-+-+-')
    print(board['mid-L'] + '|' + board['mid-M'] + '|' + board['mid-R'])
    print('-+-+-')
    print(board['low-L'] + '|' + board['low-M'] + '|' + board['low-R'])


turn = 'X'
for i in range(9):
    printBoard(theBoard)
    print('Turn for ' + turn + '. Move on which space? ')
    move = input()
    theBoard[move] = turn
    if turn == 'X':
        turn = 'O'
    else:
        turn = 'X'

printBoard(theBoard)
#+END_SRC
** Nested dictionaries and lists

#+BEGIN_SRC python

allGuests = {'Alice': {'apples': 5, 'pretzels': 12},
             'Bob': {'ham sandwiches': 3, 'apples': 2},
             'Carol': {'cups': 3, 'apple pies': 1}}


def total_brought(guests, item):
    num_brought = 0
    for k, v in guests.items():
        num_brought += v.get(item, 0)
    return num_brought

print('Number of things being brought:')
print(' - Apples ' + str(total_brought(allGuests, 'apples')))
print(' - Cups ' + str(total_brought(allGuests, 'cups')))
print(' - Cakes ' + str(total_brought(allGuests, 'cakes')))
print(' - Ham Sandwiches ' + str(total_brought(allGuests, 'ham sandwiches')))
print(' - Apple Pies ' + str(total_brought(allGuests, 'apple pies')))

#+END_SRC
** Practice projects
:PROPERTIES:
:tangle: scripts/fantasy_game.py
:END: 
*** Fantasy game inventory
You are creating a fantasy video game. The data structure to model the player's
inventory will be a dictionary where the keys are string values describing the
item in the inventory and the value is an integer value detailing how many of
that item the player has. For example, the dictionary value
~{'rope': 1, 'torch': 6, 'gold coin': 42, 'dagger': 1, 'arrow': 12}~
means the player has 1 rope, 6 torches, 42 gold coins, and so on.

Write a function named displayInventory() that would take any possible
"inventory" and display it like the following:
Inventory:
12 arrow
42 gold coin
1 rope
6 torch
1 dagger
Total number of items: 62

#+BEGIN_SRC python :results output

def display_inventory(inventory):
    """
    Prints the inventory
    """
    print("Inventory:")
    for k, v in inventory.items():
        print(v, k)
    print("Total number of items:", sum(inventory.values()))

stuff = {'rope': 1, 'torch': 6, 'gold coin': 42, 'dagger': 1, 'arrow': 12}
display_inventory(stuff)

#+END_SRC

#+RESULTS:
: Inventory:
: 6 torch
: 42 gold coin
: 12 arrow
: 1 rope
: 1 dagger
: Total number of items: 62

*** List to Dictionary Function for Fantasy Game Inventory
Imagine that a vanquished dragon's loot is represented as a list of strings
like this:
dragonLoot = ['gold coin', 'dagger', 'gold coin', 'gold coin', 'ruby']

Write a function named addToInventory(inventory, addedItems), where the
inventory parameter is a dictionary representing the player's inventory (like
in the previous project) and the addedItems parameter is a list like
dragonLoot.
The  addToInventory() function should return a dictionary that represents the
updated inventory. Note that the addedItems list can contain multiples of the
same item.

#+BEGIN_SRC python

def add_to_inventory(inventory, added_items):
    for item in added_items:
        inventory.setdefault(item, 0)
        inventory[item] += 1
    return inventory

#+END_SRC

* Manipulating strings
Text is one of the most common forms of data your programs will handle. 

** String literals

*** Double quotes
Use double quotes when you need single quotes inside a string, and the opposite.

*** Escape characters
Lets you use characters that are otherwise impossible to put into a string.
Consists of a backslash \ followed by the character.

#+BEGIN_SRC python :results output
print("Hello there!\nHow are you?\nI\'m doing fine, and you?")
#+END_SRC

#+RESULTS:
: Hello there!
: How are you?
: I'm doing fine, and you?

*** Raw strings
You can place an r before the beginning quotation mark of a string to make it a
raw string. A raw string completely ignores all escape characters and prints
any backslash that appears in the string. Useful in strings with regexp.

#+BEGIN_SRC python :results output
print(r'That is Carol\'s cat.')
#+END_SRC

#+RESULTS:
: That is Carol\'s cat.

*** Multiline strings with triple quotes
A multiline string in Python begins and ends with either three single quotes or
three double quotes.

#+BEGIN_SRC python :results output
print('''Dear Alice,
Eve's cat has been arrested for catnapping, cat burglary, and extortion.
Sincerely,
Bob''')
#+END_SRC

#+RESULTS:
: Dear Alice,
: Eve's cat has been arrested for catnapping, cat burglary, and extortion.
: Sincerely,
: Bob

*** Multiline comments
The hash character marks the beginning of a comment line. Triple quotes for
multiline comments. Better in documentation.

** Indexing and slicing strings.

** Useful methods
 
Return new string values
 - upper()
 - lower()

*** The isX methods
Return true or false. Always false if blank.
 - isupper() :
 - islower() :
 - isalpha() : if consists only of letters
 - isalnum() : 
 - isdecimal() :
 - isspace() :
 - istitle() : 

Helpful when you need to validte user input, like the next example.

#+begin_src python :tangle validate_input.py
while True:
    print('Enter your age:')
    age = input()
    if age.isdecimal():
        break
    print('Please enter a number for your age.')

while True:
    print('Select a new password (letters and numbers only):')
    password = input()
    if password.isalnum():
        break
    print('Passwords can only have letters and numbers.')
#+end_src

*** Starts and ends string methods
These methods are useful alternatives to the == equals operator if you need to
check only whether the first or last part of the string, rather than the whole
thing, is equal to another string.
 - startswith(string)
 - endswith(string)

*** Join and split
 - 'str'.join(list): concatenate a list of strings with a delimiter.
 - 'str'.split(str = ' ') : divide a list of strings by a delimiter.

*** Justify with rjust(), ljust(), center()
Return a padded version of the string they are called on, with spaces inserted
to justify the text. The first argument to both methods is an integer length
for the justified string.
 - rjust(int, char=' ')
 - ljust(int, char=' ')

#+BEGIN_SRC python :tangle scripts/picnic_table.py
#! python3 

def printPicnic(itemsDict, leftWidth, rightWidth):
    print('PICNIC ITEMS'.center(leftWidth + rightWidth, '-'))
    for k, v in itemsDict.items():
        print(k.ljust(leftWidth, '.') + str(v).rjust(rightWidth))

picnicItems = {'sandwiches': 4, 'apples': 12, 'cups': 4, 'cookies': 8000}
printPicnic(picnicItems, 12, 5)
printPicnic(picnicItems, 20, 6)
#+END_SRC

*** Removing Whitespace with strip(), rstrip(), and lstrip()
Sometimes you may want to strip off whitespace characters (space, tab, and
newline) from the left side, right side, or both sides of a string.

*** Copying and Pasting Strings with the pyperclip Module
The ~pyperclip~ module has copy() and paste() functions that can send text
to and receive text from your computer's clipboard. You can call it with
~import pyperclip~

** Project: Password Locker

#+BEGIN_SRC python :tangle scripts/password_locker/pw.py
#! python3
# pw.py - An insecure password locker program.

PASSWORDS = {'email':'F7minlBDDuvMJuxESSKHFhTxFtjVB6', 'blog':
             'VmALvQyKAxiVH5G8v01if1MLZF3sdt', 'luggage':'12345'}

import sys
import pyperclip

if len(sys.argv) < 2:
    print('Usage: python pw.py [account] - copy account password')
    sys.exit()
account = sys.argv[1] # first command line arg is the account name

if account in PASSWORDS:
    pyperclip.copy(PASSWORDS[account])
    print('Password for ' + account + ' copied to clipboard.')
else:
    print('There is no account named ' + account)

#+END_SRC

** Project: adding bullets

#+BEGIN_SRC python :tangle scripts/bulletPointAdder.py
#! python3
"""
bulletPointAdder.py - Adds Wikipedia bullet points to the start of each line of
text on the clipboard
"""

import pyperclip
# Paste text from the clipboard
text = pyperclip.paste()

# TODO: separate lines and add stars


# Copy the new text to the clipboard
pyperclip.copy(text)

#+END_SRC

* Regular expressions 
Allow to specify a pattern of text to search for.
Descriptions for a pattern of text

** Finding patterns without regexp
Say you want to find a phone number in a string. You know the pattern: three
numbers, a hyphen, three numbers, a hyphen, and four numbers.  Here's an
example: 415-555-4242

#+BEGIN_SRC python :tangle scripts/is_phone_number.py
def is_phone_number(text):
    """
    Checks wheter a string matches a phone number pattern
    Keyword Arguments:
    text -- any valid string that might represent a telephone number
    """
    # First check if text has the correct number of chars
    if len(text) != 12:
        return False
    # Check if any of the first chars is not a number
    for i in range(0, 3):
        if not text[i].isdecimal():
            return False
    # Check if 4th character is an not an hyphen
    if text[3] != '-':
        return False
    for i in range(4, 7):
        if not text[i].isdecimal():
            return False
    if text[7] != '-':
        return False
    for i in range(8, 12):
        if not text[i].isdecimal():
            return False
    return True

#+END_SRC

** Regexp in python
Use the ~re~ module. More info [[https://docs.python.org/3/howto/regex.html][here]].

Instructions:
 - Import regex module ~import re~
 - Create ~Regex~ object with ~re.compile()~. Use a raw string.
 - Pass the string into the ~Regex~ object's ~search()~ method. Returns a
   ~Match~ object.
 - Call the ~Match~ object's ~group()~ method to return a string of the actual
   matched text

#+BEGIN_SRC python :tangle scripts/regexp1.py :results output
import re

phoneNumRegex = re.compile(r'\d\d\d-\d\d\d-\d\d\d\d')
mo = phoneNumRegex.search('My number is 415-555-4242.')
print('Phone number found: ' + mo.group())

#+END_SRC

#+RESULTS:
: Phone number found: 415-555-4242

** Grouping with parentheses
To separate portions of text.
 - ~group(1)~: first set of ()
 - ~group(2)~
 - ~group(0)~: entire matching text
 - ~groups()~: returns a tuple with all groups

** Matching multiple groups with the pipe
The | char is called pipe. When you want to match one of many expressions.
